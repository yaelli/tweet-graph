'use strict';

/**
 * @ngdoc overview
 * @name twitterGraphApp
 * @description
 * # twitterGraphApp
 *
 * Main module of the application.
 */
var yaelTwitter = angular
  .module('twitterGraphApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angularCharts',
    'nvd3',
    'angularMoment'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
