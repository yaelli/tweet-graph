'use strict';

/**
 * @ngdoc function
 * @name twitterGraphApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the twitterGraphApp
 */
yaelTwitter.controller('MainCtrl', function ($scope, twitterData, localTwitter, $resource) {


  $scope.search = function(){
    localTwitter.get({q:$scope.searchTerm},
      function(data){
        manageData(data.samples);
      },
      function(err){
        alert('error');
      });
  };

  var manageData = function(dataList){
    var tempList=[];
      for (var i = 0; i<dataList.length; i++){
        var date = dataList[i].date;
        date = parseInt(date/1000/3600)*1000*3600;
        date = moment(date);
        var hasValue = _.find(tempList, { 'date': date });
        if (hasValue){
          hasValue.value++;
        }
        else{
          tempList.push({ "date" : date , "label" : tempList.length+1 , "value" : 1 });
        }
      }
    $scope.data[0].values = tempList;
  };

  $scope.data = [{
    key: "Cumulative Return",
    values: []
  }]

  $scope.config = {
    visible: true, // default: true
    extended: false, // default: false
    disabled: false, // default: false
    autorefresh: true, // default: true
    refreshDataOnly: false // default: false
  };

  $scope.options = {
    chart: {
      type: 'cumulativeLineChart',
      height: 450,
      margin : {
        top: 20,
        right: 20,
        bottom: 60,
        left: 55
      },
      x: function(d){ return d.label; },
      y: function(d){ return d.value; },
      showValues: true,
      valueFormat: function(d){
        return d3.format(',.4f')(d);
      },
      transitionDuration: 500,
      xAxis: {
        axisLabel: 'X Axis'
      },
      yAxis: {
        axisLabel: 'Y Axis',
        axisLabelDistance: 30
      }
    }
  };


});
